<?php
session_start();
  require_once "config/baseurl.php";

  if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
    echo "<div id=\"login\"><h1>Untuk mengakses modul, Anda harus login dulu.</h1>
        <p class=\"fail\"><a href=\"idaroh/index.php\">LOGIN</a></p></div>";  
  }
  else{
    if ($_SESSION['leveluser']=='user') {
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Home | BAPAK UNIDA Gontor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta http-equiv="Copyright" content="Library UNIDA Gontor">
  <meta name="author" content="Muhammad Ibrahim">
  <link rel="shortcut icon" href="dist/img/favicon.png">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?=$baseurl;?>" class="navbar-brand" title="BAPAK UNIDA Gontor"><b>BAPAK</b> UNIDA Gontor</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="persyaratan"><i class="fa fa-file-text"></i>&nbsp; Persyaratan</a></li>
            <li><a href="upload-data"><i class="fa fa-cloud-upload"></i>&nbsp; Upload Data</a></li>
            <li><a href="download-file"><i class="fa fa-download"></i>&nbsp; Download File</a></li>
            <li><a href="idaroh/logout.php"><i class="fa fa-mail-forward"></i>&nbsp; Logout</a></li>
                
          </ul>
         
        </div>
        
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper" id="formbebaspustaka">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          &nbsp;
          <small>&nbsp;</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><a href="#">Welcome</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-warning">
              <center>
                <h1 class="box-title">DATA BERHASIL DISIMPAN</h1>
                <p>- Biro Administrasi Penunjang Akademik Kemahasiswaan -</p>
              </center>
              <hr>
              <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
                <form role="form" method="post" action="">
                    <center>
                        <div class="input-group margin">
                            <input type="text" class="form-control" name="carinim" placeholder="Type NIM Here .." autofocus="" >
                                <span class="input-group-btn" >
                                  <input type="submit" name="cari" value="Cari" class="btn btn-primary btn-flat" />
                                </span>
                          </div>
                    </center>
                </form>


                <?php
                    require "config/koneksi.php";

                    $input_cari = @$_POST['carinim']; 
                    $cari = @$_POST['cari'];

                       // jika tombol cari di klik
                      if($cari) {

                          // jika kotak pencarian tidak sama dengan kosong
                          if($input_cari != "") {
                          // query mysql untuk mencari berdasarkan nim. .
                          $sql = mysqli_query($konek, "SELECT * FROM pemohon where nim like '%$input_cari%'") or die (mysqli_error()); 
                          // mengecek data
                          $cek = mysqli_num_rows($sql);  
                        

                          // jika data kurang dari 1
                     if($cek < 1) {
                      
                        ?>

                        <div class="control-group">
                            <center>
                              <h3>Data Tidak Ditemukan</h3>
                            </center>
                              
                        </div>
                        
                    <?php

                     } 
                     else {
                        
                      $r=mysqli_fetch_array($sql);
                      $aksi="idaroh/modules/mod_laporan/cetak_form.php";

                      ?>
                    <br><br><br>
                    <div class="control-group">
                        <center>
                            <h4>Data dengan NIM <b><?php echo $r['nim'] ?></b> atas Nama <b><?php echo $r['nama'] ?></b> ditemukan</h4>
                            <b>>></b>Silahkan download file anda dan simpan dengan baik<b><<</b>
                        </center>
                    </div>
                    <br>
                    <form role="form" method="post" class="form-horizontal" <?php echo "action=\"$aksi?module=laporan&act=view\""; ?>>
                        
                        <input type="hidden" name="nim" value="<?php echo $r['nim'] ?>" />
                        <center>
                            <button type="submit" class="btn btn-lg btn-success" name="download"> <i class="fa fa-download"></i> Download File</button>
                        </center>
                      
                    </form>
                 
                       
                    <?php
                     }
                          } 
                          else {
                          $sql = mysqli_query($con, "SELECT * FROM tbmahasiswa") or die (mysqli_error());
                          }
                      } 
                    ?>  


              </div><!-- /.col md 12 -->
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!--/.col (right) -->
      </section>



          <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
      </div>
      <strong>Copyright &copy; 2017</strong> by Universitas Darussalam Gontor | All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>
<?php
                }
              }
            ?>