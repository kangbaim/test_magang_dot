<?php
require_once "config/koneksi.php";
require_once "config/fungsi_antiinjection.php";
require_once "config/baseurl.php";
require_once "plugins/mpdf60/mpdf.php";

    $module = $_GET['module'];
    $act    = $_GET['act'];

if (isset($_POST['simpan'])) {
    // membuat query max
    $carikode = mysqli_query($konek, "SELECT max(kd_motor) FROM pemohon") or die (mysqli_error());
    // menjadikannya array
    $datakode = mysqli_fetch_array($carikode);
    // jika $datakode
    if ($datakode) {
        $nilaikode = substr($datakode[0], 1);
        // menjadikan $nilaikode ( int )
        $kode = (int) $nilaikode;
        // setiap $kode di tambah 1
        $kode = $kode + 1;
        $kode_otomatis = str_pad($kode, 4, "0", STR_PAD_LEFT);
    } 
    else { 
        $kode_otomatis = "0001";
    }

    $nim      = anti_injection($_POST['nim']);
    $nama     = anti_injection($_POST['nama']);
    $fak      = anti_injection($_POST['fakultas']);
    $prodi    = anti_injection($_POST['prodi']);
	$semester = anti_injection($_POST['semester']);
	$asrama   = anti_injection($_POST['asrama']);
    $kamar    = anti_injection($_POST['kamar']);
	$ttl      = anti_injection($_POST['ttl']);
	$alamat   = anti_injection($_POST['alamat']);
	$kota     = anti_injection($_POST['kota']);
	$nohp     = anti_injection($_POST['nohp']);
    $email    = anti_injection($_POST['email']);
    $namawali = anti_injection($_POST['namawali']);
    $ttlwali  = anti_injection($_POST['ttlwali']);
    $alamatwali = anti_injection($_POST['alamatwali']);
    $kotawali = anti_injection($_POST['kotawali']);
	$nohpwali = anti_injection($_POST['nohpwali']);
	$nopol    = anti_injection($_POST['nopol']);
	$merk     = anti_injection($_POST['merk']);
	$warna    = anti_injection($_POST['warna']);
	$bbm      = anti_injection($_POST['bbm']);
	$thnbuat  = anti_injection($_POST['thnbuat']);

    $nama1       = mysqli_real_escape_string($konek, $nama);
    $nama2       = mysqli_real_escape_string($konek, $namawali);
    $alamat1     = mysqli_real_escape_string($konek, $alamat);
    $alamat2     = mysqli_real_escape_string($konek, $alamatwali);


    $lok_file1   = $_FILES['fizin']['tmp_name'];
    $tipe_file1  = $_FILES['fizin']['type'];
    $nama_file1  = $_FILES['fizin']['name'];
    $ukuran1     = $_FILES['fizin']['size'];
    $eks_boleh1  = array('png','jpg');
    $a           = explode('.', $nama_file1);
    $ekstensi1   = strtolower(end($a));
    $nama_fizin  = $nim.'-'.$nama_file1;

    $lok_file2   = $_FILES['fsim']['tmp_name'];
    $tipe_file2  = $_FILES['fsim']['type'];
    $nama_file2  = $_FILES['fsim']['name'];
    $ukuran2     = $_FILES['fsim']['size'];
    $eks_boleh2  = array('png','jpg');
    $b           = explode('.', $nama_file2);
    $ekstensi2   = strtolower(end($b));    
    $nama_fsim   = $nim.'-'.$nama_file2;
    
    $lok_file3   = $_FILES['fstnk']['tmp_name'];
    $tipe_file3  = $_FILES['fstnk']['type'];
    $nama_file3  = $_FILES['fstnk']['name'];
    $ukuran3     = $_FILES['fstnk']['size'];
    $eks_boleh3  = array('png','jpg');
    $c           = explode('.', $nama_file3);
    $ekstensi3   = strtolower(end($c));
    $nama_fstnk  = $nim.'-'.$nama_file3;

    $lok_file4   = $_FILES['fmotor']['tmp_name'];
    $tipe_file4  = $_FILES['fmotor']['type'];
    $nama_file4  = $_FILES['fmotor']['name'];
    $ukuran4     = $_FILES['fmotor']['size'];
    $eks_boleh4  = array('png','jpg');
    $d           = explode('.', $nama_file4);
    $ekstensi4   = strtolower(end($d));
    $nama_fmotor = $nim.'-'.$nama_file4;
    
    //echo "$nim<br>";
    //echo "$nama<br>";
    $size=4000000;

    if((in_array($ekstensi1, $eks_boleh1) === true) AND (in_array($ekstensi2, $eks_boleh2) === true) AND (in_array($ekstensi3, $eks_boleh3) === true) AND (in_array($ekstensi4, $eks_boleh4) === true)){
        if(($ukuran1 > $size) || ($ukuran2 > $size) || ($ukuran3 > $size) || ($ukuran4 > $size)) {
            echo "<script>alert('Ukuran file tidak boleh > 10 MB'); 
                  window.location = 'upload-data'</script>";
        }
        else{
            $dir1 = "file/izin/$nama_fizin";
            $dir2 = "file/sim/$nama_fsim";
            $dir3 = "file/stnk/$nama_fstnk";
            $dir4 = "file/motor/$nama_fmotor";
            move_uploaded_file($lok_file1, "$dir1");
            move_uploaded_file($lok_file2, "$dir2");
            move_uploaded_file($lok_file3, "$dir3");
            move_uploaded_file($lok_file4, "$dir4");

            
            $input = "INSERT INTO pemohon (kd_motor, nim, nama, id_fak, id_prodi, semester, id_asrama, kamar, ttl, alamat, kota, nohp, email, namawali, ttlwali, alamatwali, kotawali, nohpwali, nopol, merk, warna, bbm, thnbuat, fizin, fsim, fstnk, fmotor) VALUES ('$kode_otomatis', '$nim', '$nama', '$fak', '$prodi', '$semester', '$asrama', '$kamar', '$ttl', '$alamat', '$kota', '$nohp', '$email', '$namawali', '$ttlwali', '$alamatwali', '$kotawali', '$nohpwali', '$nopol', '$merk', '$warna', '$bbm', '$thnbuat', '$nama_fizin', '$nama_fsim', '$nama_fstnk', '$nama_fmotor');";
            //echo "$nim<br> $nama<br> $kamar";
            $data = mysqli_query($konek, $input);
            echo "<script>alert('Data berhasil di simpan'); 
                  window.location = 'download-file'</script>";

        }
    }
    else{
        echo "<script>window.alert('Format Gambar Tidak valid , Format Gambar Harus (JPG, Jpeg, png)'); 
               window.location = 'upload-data'</script>";
    }
}


 // Define relative path from this script to mPDF
 //Beri nama file PDF hasil.

    
?>